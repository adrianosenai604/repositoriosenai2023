const {createApp} = Vue; // Importa a função createApp do Vue.js

createApp({
    data(){
        return{
            valorDisplay:"0", // Valor exibido no display da calculadora
            operador:null, // Operador atualmente selecionado
            numeroAtual:null, // Número atualmente em uso
            numeroAnterior:null, // Número anteriormente utilizado
            tamanhoLetra: 50 + "px", // Tamanho da letra no display
        };
    },

    methods:{
        getNumero(numero){
            this.ajusteTamanhoDisplay(); // Chama a função para ajustar o tamanho da letra no display

            if(this.valorDisplay == "0"){
                this.valorDisplay = numero.toString(); // Se o valor exibido for 0, substitui pelo número digitado
            }
            else{
            /*     Analisa de o operador é igual a "=" para que após uma operação, o valor numérico acionado não seja concatenado ao valor do display*/
                if(this.operador == "="){
                    this.valorDisplay = "";
                    this.operador = null;
                }

                this.valorDisplay += numero.toString(); // Concatena o número digitado ao valor exibido
            }
        },
        
        limpar(){
            this.valorDisplay = "0"; // Limpa o valor exibido
            this.operador = null; // Remove o operador selecionado
            this.numeroAnterior = null; // Remove o número anteriormente utilizado
            this.numeroAtual = null; // Remove o número atualmente em uso
            this.tamanhoLetra = 50 + "px"; // Restaura o tamanho padrão da letra no display
        },

        decimal(){
            if(!this.valorDisplay.includes(".")){
                this.valorDisplay += "."; // Adiciona um ponto decimal se ainda não estiver presente
            }
        },

        operacoes(operacao){
            /*A função é responsável por executar operações matemáticas com base em um operador selecionado.*/

            // Verifica se this.numeroAtual não é nulo.
            if(this.numeroAtual != null){
                /*Converte o valor do display atual (this.valorDisplay) para um número de ponto flutuante.*/
                const displayAtual = parseFloat(this.valorDisplay);

                if(this.operador != null){
                    /* Executa a operação com base no operador selecionado. A operação é realizada entre this.numeroAtual (valor anterior) e displayAtual (valor do display).*/
                    switch(this.operador){
                        case "+":
                            // Caso o operador seja "+", adiciona os dois valores e armazena o resultado em this.valorDisplay.
                            this.valorDisplay = (this.numeroAtual + displayAtual).toString();
                            break;
                        
                        case "-":
                            // Caso o operador seja "-", subtrai displayAtual de this.numeroAtual e armazena o resultado em this.valorDisplay.
                            this.valorDisplay = (this.numeroAtual - displayAtual).toString();
                            break;
                        
                        case "*":
                            /*Caso o operador seja "*", multiplica this.numeroAtual por displayAtual e armazena o resultado em this.valorDisplay.*/
                            this.valorDisplay = (this.numeroAtual * displayAtual).toString();
                            break;

                        case "/":
                            /*Caso o operador seja "/", verifica se displayAtual ou this.numeroAtual é igual a zero. Se for o caso, define this.valorDisplay como "Operação impossível!". Caso contrário, realiza a divisão entre this.numeroAtual e displayAtual e armazena o resultado em this.valorDisplay.*/
                            if(displayAtual == 0 || this.numeroAtual == 0){
                                this.valorDisplay = "Operação impossível!";
                            }
                            else{
                                this.valorDisplay = (this.numeroAtual / displayAtual).toString();
                            }
                            break;
                    }

                    // Atualiza this.numeroAnterior para this.numeroAtual e define this.numeroAtual como nulo.
                    this.numeroAnterior = this.numeroAtual;
                    this.numeroAtual = null;

                    // Se o operador for diferente de "=", define this.operador como nulo.                    
                    if(this.operador != "="){
                        this.operador = null;
                    }
                    
                    /*Verifica se o valor do display atual (this.valorDisplay) contém um ponto decimal. Se sim, converte o valor para um número de ponto flutuante (numDecimal) e redefine this.valorDisplay como uma string contendo o valor formatado com duas casas decimais.*/
                    if(this.valorDisplay.includes(".")){
                        const numDecimal = parseFloat(this.valorDisplay);
                        this.valorDisplay = (numDecimal.toFixed(2)).toString();
                    }                    
                }
                else{
                    // Caso contrário, define this.numeroAnterior como displayAtual.
                    this.numeroAnterior = displayAtual;
                }
            }//Fechamento if

            // Define this.operador como operacao.
            this.operador = operacao;

            /*Converte o valor do display atual (this.valorDisplay) para um número de ponto flutuante e armazena em this.numeroAtual.*/
            this.numeroAtual = parseFloat(this.valorDisplay);

            // Se o operador for diferente de "=", redefine this.valorDisplay como "0".
            if(this.operador != "="){
                this.valorDisplay = "0";
            }

            this.ajusteTamanhoDisplay(); // Chama a função para ajustar o tamanho da letra no display
        },//Fechamento operações
        
        ajusteTamanhoDisplay(){
            // Ajusta o tamanho da letra no display com base no comprimento do número exibido
            if(this.valorDisplay.length >= 31){
                this.tamanhoLetra = 14 + "px"; // Se o número exibido for muito longo, diminui o tamanho da letra
                return;                
            }
            else if(this.valorDisplay.length >= 21){
                this.tamanhoLetra = 20 + "px"; // Se o número exibido for longo, diminui o tamanho da letra
                return;
            }
            else if(this.valorDisplay.length >= 12){
                this.tamanhoLetra = 30 + "px"; // Se o número exibido for médio, diminui o tamanho da letra
            }
            else{
                this.tamanhoLetra = 50 + "px"; // Caso contrário, usa o tamanho padrão da letra
            }
        },//Fechamento ajusteTamanhoDisplay
    },Fechamento methods

}).mount("#app"); // Monta a aplicação Vue.js no elemento HTML com o ID "app"